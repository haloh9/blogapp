import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
 
import { LoginComponent } from './login/login.component';
import { RootComponent } from './root/root.component';
import { HomeComponent } from './home/home.component';
import { PostComponent } from './post/post.component';

export const AppRoutes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: '/home' },
    { path: 'home', pathMatch: 'full', component: HomeComponent},
    { path: 'login', pathMatch: 'full', component: LoginComponent},
    { path: 'post/show', pathMatch: 'full', redirectTo: '/home'},
    { path: 'post/edit', pathMatch: 'full', redirectTo: '/home'},
    { path: 'post/remove', pathMatch: 'full', redirectTo: '/home'},
    { path: 'post/add', pathMatch: 'full', component: PostComponent},
    { path: 'post/show/:id', pathMatch: 'full', component: PostComponent},
    { path: 'post/edit/:id', pathMatch: 'full', component: PostComponent},
    { path: 'post/remove/:id', pathMatch: 'full', component: PostComponent}


];
 
export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);