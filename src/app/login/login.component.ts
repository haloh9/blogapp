import { Component } from '@angular/core';
import { User } from '../shared/models/user.model';
import { UserService } from '../shared/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
 
  public user : User;
 
  constructor(private userService: UserService, private router: Router) {
      this.user = new User();
  }
 
  validateLogin() {
  	if(this.user.username && this.user.password) {
      this.userService.validateLogin(this.user);
      this.router.navigateByUrl('/');
  	} else {
  		alert('enter user name and password');
  	}
  }
 
}