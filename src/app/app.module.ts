import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ROUTING } from './app.routing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { RootComponent } from './root/root.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './template/header/header.component';
import { FooterComponent } from './template/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { CategoryService } from './shared/services/category.service';
import { UserService } from './shared/services/user.service';
import { PostService } from './shared/services/post.service';
import { ShowListComponent } from './post/show-list/show-list.component';
import { PostComponent } from './post/post.component';


@NgModule({
  declarations: [
    RootComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ShowListComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    ROUTING,
    FormsModule,
    HttpClientModule,
    AngularEditorModule
  ],
  providers: [UserService, CategoryService, PostService],
  bootstrap: [RootComponent]
})
export class AppModule { }
