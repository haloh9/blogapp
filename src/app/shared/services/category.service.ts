import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private listOfCategories: any;

  constructor(private http: HttpClient) {

  }


  getAllCategories() {

    this.http.get('http://localhost:3000/categories').subscribe(result => {
      this.listOfCategories = result;

    }, error => {
      console.log('error is ', error);
    });

    return {"categories": [
      {
        "id": 1,
        "name": "Acceuil"
      },
      {
        "id": 2,
        "name": "Politique"
      },
      {
        "id": 3,
        "name": "Economie"
      },
      {
        "id": 4,
        "name": "Science"
      },
      {
        "id": 4,
        "name": "Tech"
      },
      {
        "id": 6,
        "name": "Sport"
      }
    ],};
  }


}