import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../../shared/models/post.model';
import { User } from '../models/user.model';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  constructor(private http: HttpClient, private userService: UserService) { }

  getAllPost() {
    return this.http.get('http://localhost:3000/posts/');
  }

  getPost(id: any) {
    return this.http.get('http://localhost:3000/posts/' + id);
  }

  addPost(post: Post, user: User) {
    return this.http.post('http://localhost:3000/posts/', {
      title: post.title,
      content: post.content,
      author: user.username,
      id_category: post.id_category,
      created_at: new Date(),
    })
  }

  updatePost(post: Post, user: User) {
    return this.http.put('http://localhost:3000/posts/' + post.id, {
      id: post.id,
      title: post.title,
      content: post.content,
      author: user.username,
      created_at: post.created_at,
      id_category: post.id_category,
      updated_at: new Date()
    })
  }

  removePost(id: any) {
    return this.http.delete('http://localhost:3000/posts/' + id);
  }


  



}
