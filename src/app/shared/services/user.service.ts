import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { Router } from '@angular/router';

@Injectable()
export class UserService {

    private userData: User;

    constructor(private http: HttpClient, private router: Router){

	}
	
	validateLogin(user: User){
        this.http.get('http://localhost:3000/login/').subscribe(result => {
            if(result['status'] == 'success')
                this.userData = result['data'];

        }, error => {
            console.log('error is ', error);
        });
  
    }
    
    logoutUser(){
        this.userData = null;
        this.router.navigateByUrl('/');

    }
    getUser(){
        return this.userData;
    }
}