export class User {
    constructor(){
        this.username = '';
        this.firstname = '';
        this.lastname = '';
        this.email = '';
        this.password = '';
    }
    public username;
    public firstname;
    public lastname;
    public email;
    public password;
}