export class Post {
	constructor(data: any) {
		if (data) {
			this.id = data.id;
			this.author = data.author;
			this.title = data.title;
			this.content = data.content;
			this.created_at = data.created_at;
			this.count_comments = data.count_comments;
			this.id_category = data.id_category;
		}

	}
	public id;
	public author;
	public title;
	public content;
	public created_at;
	public count_comments;
	public id_category;
}