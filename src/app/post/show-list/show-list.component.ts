import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { PostService } from 'src/app/shared/services/post.service';
import { Post } from 'src/app/shared/models/post.model';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-show-list',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.css']
})
export class ShowListComponent implements OnInit {
  public posts: any[];
  @Input() idCategory: any;

  constructor(private postService: PostService, private router: Router, private userService: UserService) {
  }


  showPost(id: any) {
    this.router.navigateByUrl('/post/show/' + id);
  }
  ngOnInit() {
    this.getAllPost();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.idCategory = changes.idCategory.currentValue;
    this.getAllPost();
  }

  getAllPost() {
    console.log(this.idCategory);
    this.posts = new Array();
    this.postService.getAllPost().subscribe(result => {
      for (let i = 0; i < result['length']; i++) {
        result[i]['content'] = result[i]['content'].substring(0, 1000) + '....(voir la suite)';
        if ((this.idCategory == 1))
          this.posts.push(result[i]);
        else {
          if (result[i]['id_category'] == this.idCategory)
            this.posts.push(result[i]);
        }
      }
      console.log(this.posts)
    }, error => {
    });
  }



}
