import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Post } from '../shared/models/post.model';
import { PostService } from '../shared/services/post.service';
import { UserService } from '../shared/services/user.service';
import { User } from '../shared/models/user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from '../shared/services/category.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  public post: Post;
  public user: User;
  private showPost: Boolean;
  private editPost: Boolean;
  public isOwner: Boolean;
  public listOfCategories: any;

  constructor(private postService: PostService, private userService: UserService, private router: Router, private _route: ActivatedRoute, private categoryService: CategoryService) {
    this.post = new Post(false);
    this.isOwner = false;
    this.showPost = false;
    this.editPost = false;
    this.listOfCategories = this.categoryService.getAllCategories()['categories'];
    this.post.id_category = 0;
  }

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  }

  ngOnInit() {
    let idPost = this._route.snapshot.params['id'];
    this.user = this.userService.getUser();

    if (this.router.url.includes('show')) {
      this.showThePost(idPost);
      this.showPost = true;
      this.editPost = false;
    } else if(this.router.url.includes('add')) {
      if(this.user){
        this.showPost = false;
        this.editPost = false;
      } else {
        this.router.navigateByUrl('/');
      }
    } else if(this.router.url.includes('edit')){
      this.showThePost(idPost);
      this.showPost = false;
      this.editPost = true;
    } else if(this.router.url.includes('remove')){
      this.removeThePost(idPost);
    }
  }

  editThePost(id: any) {
    this.showPost = false;
    this.editPost = true;
  }

  removeThePost(id: any) {
    this.postService.removePost(id).subscribe(res => {
      this.router.navigateByUrl('/');
    });
  }

  showThePost(idPost) {
    this.user = this.userService.getUser();

    this.postService.getPost(idPost).subscribe(result => {
      this.post.id = result['id'];
      this.post.author = result['author'];
      this.post.title = result['title'];
      this.post.content = result['content'];
      this.post.created_at = result['created_at'];
      this.post.id_category = result['id_category'];
      if(this.user){
        if(this.user['username'] == this.post.author)
          this.isOwner = true;
      }

    }, error => {
      this.router.navigateByUrl('/');
    });
  }

  validateAddForm() {
    console.log(this.post.id_category);
    if (this.post.title && this.post.content && (this.post.id_category != '0')) {
      this.user = this.userService.getUser();
      if (this.editPost) {
        this.postService.updatePost(this.post, this.user).subscribe(res => {
          this.showThePost(this.post.id);
          this.editPost = false;
          this.showPost = true;
        });
      } else {
        this.postService.addPost(this.post, this.user).subscribe(res => {
          if (res) {
            this.post.id = res['id'];
            this.post.author = res['author'];
            this.post.title = res['title'];
            this.post.content = res['content'];
            this.post.created_at = res['created_at'];
            this.post.id_category = res['id_category'];
            this.showThePost(this.post.id);
            this.showPost = true;
          }
        });
      }

    } else {
      alert('Le titre, le contenu et la catégorie sont requis !');
    }
  }

}
