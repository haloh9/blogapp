import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { CategoryService } from '../shared/services/category.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public idCategory: any;
  public listOfCategories: any;

  constructor(private userService: UserService, private categoryService: CategoryService) { 
    this.idCategory = 1;
  }

  ngOnInit() {
    this.listOfCategories = this.categoryService.getAllCategories()['categories'];

  }

  showPostByCategory(idCategory: any){
    this.idCategory = idCategory;
  }
}
