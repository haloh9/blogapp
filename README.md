#### Required
Git - npm - json-server (pour simuler l'api)

#### Clone the repository
```sh
$ git clone https://haloh9@bitbucket.org/haloh9/blogapp.git
```

#### Install

```sh
$ npm install -g json-server
$ cd blogapp/
/blogapp$ npm install
```

#### Start

```sh
$ cd /blogapp
/blogapp $ json-server --watch blog.db.json /* simulation api sur localhost:3000 */
/blogapp $ ng serv /* localhost:4200 */
```

Afin de se connecter et pouvoir ajouter des articles :
⋅⋅* username :user 
⋅⋅* paswword : user
L'inscription n'a pas encore été faite.